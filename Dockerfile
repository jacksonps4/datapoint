FROM maven:3-adoptopenjdk-11 as builder
ADD . /opt/datapoint
WORKDIR /opt/datapoint
RUN mvn package

FROM adoptopenjdk:11-jre-hotspot
COPY --from=builder /opt/datapoint/target/datapoint*.jar /opt/app/deployment/datapoint.jar
ENTRYPOINT [ "java", "-jar", "/opt/app/deployment/datapoint.jar" ]
