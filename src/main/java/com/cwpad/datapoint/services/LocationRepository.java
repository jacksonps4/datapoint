package com.cwpad.datapoint.services;

import com.cwpad.datapoint.entity.Location;

import java.util.List;

public interface LocationRepository {
	void save(com.cwpad.datapoint.api.Location location);
	void save(List<com.cwpad.datapoint.api.Location> locations);
	com.cwpad.datapoint.api.Location find(long id);
	List<com.cwpad.datapoint.api.Location> findByNameText(String locationText);
	List<com.cwpad.datapoint.api.Location> all();

	void saveEntity(List<Location> locations);
}
