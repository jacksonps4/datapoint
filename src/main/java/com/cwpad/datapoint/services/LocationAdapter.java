package com.cwpad.datapoint.services;

import com.cwpad.datapoint.api.Location;

public class LocationAdapter {
    public Location toApi(com.cwpad.datapoint.entity.Location e) {
        if (e == null) {
            return null;
        }
        Location l = new Location();
        l.setContinent(e.getContinent());
        l.setCountry(e.getCountry());
        l.setElevation(e.getElevation());
        l.setI(e.getI());
        l.setId(e.getId());
        l.setLat(e.getLat());
        l.setLon(e.getLon());
        l.setName(e.getName());
        return l;
    }
}
