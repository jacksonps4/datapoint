package com.cwpad.datapoint.services;

import com.cwpad.datapoint.api.Location;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
public class LocationRepositoryImpl implements LocationRepository {
    @PersistenceContext
    EntityManager entityManager;

    public LocationRepositoryImpl() {
    }

    LocationRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    private final LocationAdapter locationAdapter = new LocationAdapter();

    @Override
    public void save(Location location) {
        entityManager.persist(location);
    }

    @Override
    public void saveEntity(List<com.cwpad.datapoint.entity.Location> locations) {
        for (com.cwpad.datapoint.entity.Location location : locations) {
            entityManager.persist(location);
        }
    }

    @Override
    public void save(List<Location> locations) {
        for (Location location : locations) {
            save(location);
        }
    }

    @Override
    public Location find(long id) {
        return locationAdapter.toApi(entityManager.find(com.cwpad.datapoint.entity.Location.class, id));
    }

    @Override
    public List<Location> findByNameText(String locationText) {
        return entityManager.createQuery("select l from Location l" + " where l.name like ?1",
                com.cwpad.datapoint.entity.Location.class)
                .setParameter(1, String.format("%%%s%%", locationText))
                .getResultList()
                .stream()
                .map(locationAdapter::toApi)
                .collect(Collectors.toList());
    }

    @Override
    public List<Location> all() {
        return entityManager.createQuery("select l from Location l", com.cwpad.datapoint.entity.Location.class)
                .getResultList()
                .stream()
                .map(locationAdapter::toApi)
                .collect(Collectors.toList());
    }
}
