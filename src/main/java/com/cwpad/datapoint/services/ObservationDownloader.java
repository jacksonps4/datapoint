package com.cwpad.datapoint.services;

import com.cwpad.datapoint.entity.Location;
import com.cwpad.datapoint.entity.WeatherObservation;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.annotation.Schedules;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

public class ObservationDownloader {
	public static final String DATAPOINT_OBS_URL_KEY = "datapoint.obs.url";

	private final Logger logger = Logger.getLogger(getClass().getSimpleName());

	private final LocationRepository locationRepository;
	private final WeatherObservationRepository weatherObservationRepository;
	private final Properties config;

	public ObservationDownloader(LocationRepository locationRepository, WeatherObservationRepository weatherObservationRepository, Properties config) {
		this.locationRepository = locationRepository;
		this.weatherObservationRepository = weatherObservationRepository;
		this.config = config;
	}

	@Schedules({ @Scheduled(fixedRate = 3600000L ) })
	public void parse() throws IOException {
		try {
			parse(config.getProperty(DATAPOINT_OBS_URL_KEY));
		} catch (Exception e) {
			logger.severe("Failed to execute Datapoint refresh: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void parse(String url) throws IOException {
		if (url == null) {
			url = config.getProperty(DATAPOINT_OBS_URL_KEY);
		}
		URL obsUrl = new URL(url);
		logger.info("Starting Datapoint observation downloader: " + obsUrl.toExternalForm());

		int locationCount = 0;
		int observationCount = 0;
		List<WeatherObservation> observations = new LinkedList<>();
		List<Location> locations = new LinkedList<>();
		try (InputStream in = obsUrl.openStream()) {
			DatapointObservationParser parser = new DatapointObservationParser(
					new InputStreamReader(in));
			for (WeatherObservation observation : parser.parse()) {
				Location location = observation.getLocation();
				if (locationRepository.find(location.getId()) == null) {
					locations.add(location);
					locationCount++;
				}
				if (weatherObservationRepository.find(location.getId(), observation.getTime()) == null) {
					observations.add(observation);
				}
				observationCount++;
			}
		}

		locationRepository.saveEntity(locations);
		weatherObservationRepository.saveEntity(observations);

		logger.info(String
				.format("Datapoint observation completed: "
						+ "%d locations added, %d observations added",
						locationCount, observationCount));
	}
}
