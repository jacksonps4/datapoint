package com.cwpad.datapoint.services;

import com.cwpad.datapoint.api.WeatherObservation;

import java.time.LocalDateTime;
import java.util.List;

public interface WeatherObservationRepository {
	void save(WeatherObservation observation);
	void save(List<WeatherObservation> observations);
	List<WeatherObservation> last48Hours(long location);
	WeatherObservation find(long location, LocalDateTime time);
	WeatherObservation latest(long location);

	void saveEntity(List<com.cwpad.datapoint.entity.WeatherObservation> observations);
}
