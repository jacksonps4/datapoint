package com.cwpad.datapoint.services;

import com.cwpad.datapoint.api.WeatherObservation;

public class ObservationAdapter {
    private final LocationAdapter locationAdapter = new LocationAdapter();

    public WeatherObservation toApi(com.cwpad.datapoint.entity.WeatherObservation e) {
        if (e == null) {
            return null;
        }
        WeatherObservation o = new WeatherObservation();
        o.setDewPoint(e.getDewPoint());
        o.setLocation(locationAdapter.toApi(e.getLocation()));
        o.setPressure(e.getPressure());
        o.setPressureTendency(e.getPressureTendency());
        o.setScreenRelativeHumidity(e.getScreenRelativeHumidity());
        o.setTemperature(e.getTemperature());
        o.setTime(e.getTime());
        o.setVisibility(e.getVisibility());
        o.setWeatherType(e.getWeatherType());
        o.setWindDirection(e.getWindDirection());
        o.setWindGust(e.getWindGust());
        o.setWindSpeed(e.getWindSpeed());
        return o;
    }
}
