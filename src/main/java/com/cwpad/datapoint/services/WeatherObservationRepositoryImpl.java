package com.cwpad.datapoint.services;

import com.cwpad.datapoint.api.WeatherObservation;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
public class WeatherObservationRepositoryImpl implements WeatherObservationRepository {
	@PersistenceContext
	EntityManager em;

	private final ObservationAdapter observationAdapter = new ObservationAdapter();

	public WeatherObservationRepositoryImpl() {
		super();
	}

	WeatherObservationRepositoryImpl(EntityManager em) {
		this.em = em;
	}

	@Override
	public void save(WeatherObservation observation) {
		em.persist(observation);
	}

	@Override
	public void saveEntity(List<com.cwpad.datapoint.entity.WeatherObservation> observations) {
		for (com.cwpad.datapoint.entity.WeatherObservation observation : observations) {
			em.persist(observation);
		}
	}

	@Override
	public void save(List<WeatherObservation> observations) {
		for (WeatherObservation observation : observations) {
			save(observation);
		}
	}

	@Override
	public List<WeatherObservation> last48Hours(long location) {
		LocalDateTime limit = LocalDateTime.now().minus(2, ChronoUnit.DAYS);
		return em.createQuery("select o from WeatherObservation o"
				+ " where o.location.id = ?1 and o.time > ?2",
				com.cwpad.datapoint.entity.WeatherObservation.class)
				.setParameter(1, location)
				.setParameter(2, limit)
				.getResultList()
				.stream()
				.map(observationAdapter::toApi)
				.collect(Collectors.toList());
	}

	@Override
	public WeatherObservation find(long location, LocalDateTime time) {
		List<WeatherObservation> observations =
				em.createQuery("select o from WeatherObservation o"
				+ " where o.location.id = ?1 and o.time = ?2",
						com.cwpad.datapoint.entity.WeatherObservation.class)
				.setParameter(1, location)
				.setParameter(2, time)
				.getResultList()
				.stream()
				.map(observationAdapter::toApi)
				.collect(Collectors.toList());
		return observations != null && observations.size() > 0 ? observations.get(0) : null;
	}

	@Override
	public WeatherObservation latest(long location) {
		List<WeatherObservation> obs = em.createQuery("select o from WeatherObservation o"
						+ " where o.location.id = ?1 order by o.time desc",
				com.cwpad.datapoint.entity.WeatherObservation.class)
				.setParameter(1, location)
				.setMaxResults(1)
				.getResultList()
				.stream()
				.map(observationAdapter::toApi)
				.collect(Collectors.toList());
		return obs.size() > 0 ? obs.get(0) : null;
	}
}
