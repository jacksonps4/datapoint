package com.cwpad.datapoint.services;

import com.cwpad.datapoint.entity.Location;
import com.cwpad.datapoint.entity.WeatherObservation;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.*;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class DatapointObservationParser {
	private final Reader jsonReader;

	public DatapointObservationParser(Reader reader) {
		super();
		this.jsonReader = reader;
	}

	public DatapointObservationParser(String json) {
		super();
		this.jsonReader = new StringReader(json);
	}

	public List<WeatherObservation> parse() {
		List<WeatherObservation> all = new LinkedList<WeatherObservation>();

		ObjectMapper om = new ObjectMapper();

		try {
			JsonNode top = om.readTree(jsonReader);

			JsonNode siteRep = top.get("SiteRep");
			JsonNode dv = siteRep.get("DV");
			JsonNode wx = siteRep.get("Wx");

			IntrospectionUtil introspector;
			IntrospectionUtil locationIntrospector;
			try {
				introspector = new IntrospectionUtil(WeatherObservation.class);
				locationIntrospector = new IntrospectionUtil(Location.class);
			} catch (IntrospectionException e) {
				throw new RuntimeException(e);
			}

			ArrayNode params = (ArrayNode) wx.get("Param");
			Map<String, String> jsonKeyToPropertyNameMap = new HashMap<>();
			for (JsonNode param : params) {
				ObjectNode obj = (ObjectNode) param;
				String key = obj.get("name").asText();
				String value = obj.get("$").asText();
				jsonKeyToPropertyNameMap
						.put(key, StringUtils.anyToCamelCase(value));
			}

			ArrayNode locations = getJsonObjectOrArray(dv.get("Location"));
			for (JsonNode location : locations) {
				Location l = new Location();
				ObjectNode loc = (ObjectNode) location;
				Map<String, Object> locProps = new HashMap<>();
				String key = null;
				for (Iterator<String> itr = loc.fieldNames(); itr.hasNext(); ) {
					key = itr.next();
					if (key.equalsIgnoreCase("Period")) {
						continue;
					}
					JsonNode value = loc.get(key);
					locProps.put(key, getJsonValue(value));
				}
				locationIntrospector.mapToProperties(l, locProps);

				ArrayNode periods = getJsonObjectOrArray(loc.get("Period"));
				for (JsonNode period : periods) {
					ObjectNode periodObj = (ObjectNode) period;
					LocalDate time = LocalDate.parse(periodObj.get("value").asText(),
							DateTimeFormatter.ISO_OFFSET_DATE);

					if (!periodObj.has("Rep")) {
						continue;
					}
					ArrayNode reports = getJsonObjectOrArray(periodObj.get("Rep"));
					for (JsonNode report : reports) {
						if (!(report instanceof ObjectNode)) {
							continue;
						}
						ObjectNode reportObj = (ObjectNode) report;
						Map<String, Object> properties = new HashMap<>();
						int offset = 0;
						for (Iterator<String> itr = reportObj.fieldNames(); itr.hasNext(); ) {
							key = itr.next();
							JsonNode value = reportObj.get(key);
							if (key.equals("$")) {
								offset = Integer.parseInt((String) getJsonValue(value));
							} else {
								properties.put(jsonKeyToPropertyNameMap.get(key),
										getJsonValue(value));
							}
						}
						properties.put(
								"time",
								LocalDateTime.of(time, LocalTime.MIDNIGHT)
										.plus(offset, ChronoUnit.MINUTES)
										.toString());

						WeatherObservation obs = new WeatherObservation();
						obs.setLocation(l);
						introspector.mapToProperties(obs, properties);
						all.add(obs);
					}
				}
			}
			return all;

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	static Object getJsonValue(JsonNode value) {
		switch (value.getNodeType()) {
		case BOOLEAN:
				return value.asBoolean();
		case NUMBER:
			NumericNode num = (NumericNode) value;
			if (!num.isIntegralNumber()) {
				return num.doubleValue();
			} else {
				long v = num.longValue();
				if (v > Integer.MAX_VALUE) {
					return new Long(v);
				}
				return num.intValue();
			}
		case STRING:
			return value.asText();
		case NULL:
			return null;
		case ARRAY:
		case OBJECT:
			throw new IllegalArgumentException();
		}
		return null;
	}

	private ArrayNode getJsonObjectOrArray(JsonNode reports) {
		if (reports.getNodeType() == JsonNodeType.OBJECT) {
			return JsonNodeFactory.instance.arrayNode(1)
					.add(reports);
		} else if (reports.getNodeType() == JsonNodeType.ARRAY) {
			return (ArrayNode) reports;
		} else {
			throw new IllegalArgumentException();
		}
	}
}
