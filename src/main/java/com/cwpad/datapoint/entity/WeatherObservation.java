package com.cwpad.datapoint.entity;

import com.cwpad.datapoint.api.PressureTendency;
import com.cwpad.datapoint.api.Visibility;
import com.cwpad.datapoint.api.WeatherType;
import com.cwpad.datapoint.api.WindDirection;

import javax.persistence.*;
import java.time.LocalDateTime;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
public class WeatherObservation {
	@Id
	@GeneratedValue(strategy=IDENTITY)
	private long id;
	private LocalDateTime time;
	@ManyToOne
	private Location location;
	private int windGust;
	private double temperature;
	@Enumerated(EnumType.STRING)
	private Visibility visibility;
	@Enumerated(EnumType.STRING)
	private WindDirection windDirection;
	private int windSpeed;
	@Enumerated(EnumType.STRING)
	private WeatherType weatherType;
	private int pressure;
	@Enumerated(EnumType.STRING)
	private PressureTendency pressureTendency;
	private double dewPoint;
	private double screenRelativeHumidity;
	
	public WeatherObservation() {
		super();
	}

	public LocalDateTime getTime() {
		return time;
	}

	public void setTime(LocalDateTime time) {
		this.time = time;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public int getWindGust() {
		return windGust;
	}

	public void setWindGust(int windGust) {
		this.windGust = windGust;
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	public Visibility getVisibility() {
		return visibility;
	}

	public void setVisibility(Visibility visibility) {
		this.visibility = visibility;
	}

	public String getVisibilityDescription() {
		return visibility != null ? visibility.getDescription() : null;
	}

	private void setVisibilityDescription(String visibilityDescription) { /* noop */ }

	public WindDirection getWindDirection() {
		return windDirection;
	}

	public void setWindDirection(WindDirection windDirection) {
		this.windDirection = windDirection;
	}

	public int getWindSpeed() {
		return windSpeed;
	}

	public void setWindSpeed(int windSpeed) {
		this.windSpeed = windSpeed;
	}

	public WeatherType getWeatherType() {
		return weatherType;
	}

	public void setWeatherType(WeatherType weatherType) {
		this.weatherType = weatherType;
	}

	public String getWeatherTypeDescription() {
		return weatherType != null ? weatherType.getDescription() : null;
	}

	private void setWeatherTypeDescription(String weatherTypeDescription) { /* noop */ }

	public int getPressure() {
		return pressure;
	}

	public void setPressure(int pressure) {
		this.pressure = pressure;
	}

	public double getPressureInHg() {
		return pressure * 0.02953;
	}

	private void setPressureInHg(double pressureInHg) { /* noop */ }

	public PressureTendency getPressureTendency() {
		return pressureTendency;
	}

	public void setPressureTendency(PressureTendency pressureTendency) {
		this.pressureTendency = pressureTendency;
	}

	public double getDewPoint() {
		return dewPoint;
	}

	public void setDewPoint(double dewPoint) {
		this.dewPoint = dewPoint;
	}

	public double getScreenRelativeHumidity() {
		return screenRelativeHumidity;
	}

	public void setScreenRelativeHumidity(double screenRelativeHumidity) {
		this.screenRelativeHumidity = screenRelativeHumidity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WeatherObservation other = (WeatherObservation) obj;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		return true;
	}
}
