package com.cwpad.datapoint.web;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.cwpad.datapoint.services.LocationRepository;
import com.cwpad.datapoint.api.Location;

@Path("loc")
@Produces(APPLICATION_JSON)
public class LocationResource {
	@Inject
    LocationRepository locationRepository;
	
	@GET
	@Path("search")
	public Response searchLocation(@QueryParam("name") String locationText) {
		if (locationText.length() < 2) {
			return Response.noContent().build();
		}
		
		List<Location> locations = locationRepository.findByNameText(locationText);
		return Response.ok().entity(locations).build();
	}

	@GET
	public List<Location> locations() {
		return locationRepository.all();
	}
}
