package com.cwpad.datapoint.web;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
public class DatapointRootResources extends ResourceConfig {
	public DatapointRootResources() {
		register(ObservationResource.class);
		register(LocationResource.class);
		register(CorsHeaderProvider.class);
	}
}
