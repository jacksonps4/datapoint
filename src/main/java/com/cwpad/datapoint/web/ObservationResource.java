package com.cwpad.datapoint.web;

import com.cwpad.datapoint.api.WeatherObservation;
import com.cwpad.datapoint.services.ObservationDownloader;
import com.cwpad.datapoint.services.WeatherObservationRepository;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.List;

@Path("obs")
@Produces(MediaType.APPLICATION_JSON)
public class ObservationResource {
	@Inject
	WeatherObservationRepository observationRepository;
	
	@Inject
	ObservationDownloader observationDownloader;

	@GET
	@Path("last48hours.json")
	public List<WeatherObservation> getObservations(@QueryParam("location") long location) {
		return observationRepository.last48Hours(location);
	}

	@GET
	@Path("latest.json")
	public WeatherObservation latest(@QueryParam("location") long location) {
		return observationRepository.latest(location);
	}

	@GET
	@Path("get_latest")
	public void getLatest(@QueryParam("url") String url) throws IOException {
		observationDownloader.parse(url);
	}
}
