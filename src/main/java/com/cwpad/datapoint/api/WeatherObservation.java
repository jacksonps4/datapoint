package com.cwpad.datapoint.api;

import java.time.LocalDateTime;

public class WeatherObservation {
	private long id;
	private LocalDateTime time;
	private Location location;
	private int windGust;
	private double temperature;
	private Visibility visibility;
	private WindDirection windDirection;
	private int windSpeed;
	private WeatherType weatherType;
	private int pressure;
	private PressureTendency pressureTendency;
	private double dewPoint;
	private double screenRelativeHumidity;
	
	public WeatherObservation() {
		super();
	}

	public LocalDateTime getTime() {
		return time;
	}

	public void setTime(LocalDateTime time) {
		this.time = time;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public int getWindGust() {
		return windGust;
	}

	public void setWindGust(int windGust) {
		this.windGust = windGust;
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	public Visibility getVisibility() {
		return visibility;
	}

	public void setVisibility(Visibility visibility) {
		this.visibility = visibility;
	}

	public String getVisibilityDescription() {
		return visibility != null ? visibility.getDescription() : null;
	}

	private void setVisibilityDescription(String visibilityDescription) { /* noop */ }

	public WindDirection getWindDirection() {
		return windDirection;
	}

	public void setWindDirection(WindDirection windDirection) {
		this.windDirection = windDirection;
	}

	public int getWindSpeed() {
		return windSpeed;
	}

	public void setWindSpeed(int windSpeed) {
		this.windSpeed = windSpeed;
	}

	public WeatherType getWeatherType() {
		return weatherType;
	}

	public void setWeatherType(WeatherType weatherType) {
		this.weatherType = weatherType;
	}

	public String getWeatherTypeDescription() {
		return weatherType != null ? weatherType.getDescription() : null;
	}

	public int getPressure() {
		return pressure;
	}

	public void setPressure(int pressure) {
		this.pressure = pressure;
	}

	public double getPressureInHg() {
		return pressure * 0.02953;
	}

	public PressureTendency getPressureTendency() {
		return pressureTendency;
	}

	public void setPressureTendency(PressureTendency pressureTendency) {
		this.pressureTendency = pressureTendency;
	}

	public double getDewPoint() {
		return dewPoint;
	}

	public void setDewPoint(double dewPoint) {
		this.dewPoint = dewPoint;
	}

	public double getScreenRelativeHumidity() {
		return screenRelativeHumidity;
	}

	public void setScreenRelativeHumidity(double screenRelativeHumidity) {
		this.screenRelativeHumidity = screenRelativeHumidity;
	}
}
