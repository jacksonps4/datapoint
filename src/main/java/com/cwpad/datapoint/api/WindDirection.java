package com.cwpad.datapoint.api;

public enum WindDirection {
	N, S, E, W,
	NE, NW, SE, SW,
	NNE, ENE, ESE, SSE,
	NNW, WNW, WSW, SSW
}