package com.cwpad.datapoint.api;

import com.cwpad.datapoint.entity.Location;

import java.time.LocalDateTime;


public class WeatherForecast {
	private Location location;
	private LocalDateTime forecastTime;
	private int feelsLike;
	private int windGust;
	private int screenRelativeHumidity;
	private int temperature;
	private Visibility visibility;
	private WindDirection windDirection;
	private int windSpeed;
	private int maxUvIndex;
	private WeatherType weatherType;
	private int precipitationProbability;
	
}
