package com.cwpad.datapoint.api;

public enum PressureTendency {
	F, R, S;

	@Override
	public String toString() {
		switch (this) {
		case F:
			return "Falling";
		case R:
			return "Rising";
		case S:
			return "Static";
		default:
			throw new IllegalArgumentException();
		}
	}

}
