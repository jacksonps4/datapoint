package com.cwpad.datapoint.api;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public enum Visibility {
	UN("Unknown", 0, 0), VP("Very poor", 0, 1000), PO("Poor", 1000, 4000), MO(
			"Moderate", 4000, 10000), GO("Good", 10000, 20000), VG("Very good", 20000, 40000), EX(
			"Excellent", 40000, Integer.MAX_VALUE);
	private static final Pattern NUMBER = Pattern.compile("[0-9]+");
	private final String name;
	private final int min;
	private final int max;

	private Visibility(String name, int min, int max) {
		this.name = name;
		this.min = min;
		this.max = max;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		if (min == 0 && max == 0) {
			return "Unknown";
		} else if (min == 0) {
			return String.format("Less than %d", max);
		} else if (max == Integer.MAX_VALUE) {
			return String.format("More than %d", min);
		} else {
			return String.format("Between %d-%dkm", min, max);
		}
	}

	public static Visibility parse(String value) {
		Matcher matcher = NUMBER.matcher(value);
		if (matcher.matches()) {
			int vis = Integer.parseInt(value);
			
			for (Visibility v : Visibility.values()) {
				if (vis < v.max && vis >= v.min) {
					return v;
				}
			}
			
			for (Visibility v : Visibility.values()) {
				if (v.min == 0 && v.max == 0) {
					continue;
				}
				if (v.min == 0 && vis < v.max) {
					return v;
				}
				if (v.max == Integer.MAX_VALUE && vis > v.min) {
					return v;
				}
			}
			
			throw new IllegalArgumentException();
		} else {
			return Visibility.valueOf(value);
		}
	}
}