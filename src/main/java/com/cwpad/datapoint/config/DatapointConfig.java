package com.cwpad.datapoint.config;

import com.cwpad.datapoint.services.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Properties;

@Configuration
@EnableScheduling
@EnableJpaRepositories
@EnableTransactionManagement
public class DatapointConfig {
    @Bean
    public Properties properties() {
        Properties props = new Properties();
        props.setProperty(
                ObservationDownloader.DATAPOINT_OBS_URL_KEY,
                "http://datapoint.metoffice.gov.uk/public/data/"
                        + "val/wxobs/all/json/all?res=hourly&key=5123c563-0cbc-46df-84d1-1e9595e692bf");
        return props;
    }

    @Bean
    public LocationRepository locationRepository() {
        return new LocationRepositoryImpl();
    }

    @Bean
    public WeatherObservationRepository weatherObservationRepository() {
        return new WeatherObservationRepositoryImpl();
    }

    @Bean
    public ObservationDownloader observationDownloader(LocationRepository locationRepository,
                                                       WeatherObservationRepository weatherObservationRepository,
                                                       Properties properties) {
        return new ObservationDownloader(locationRepository, weatherObservationRepository, properties);
    }
}
