package com.cwpad.datapoint;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatapointApplication {
    public static void main(String[] args) {
        SpringApplication.run(DatapointApplication.class, args);
    }
}
