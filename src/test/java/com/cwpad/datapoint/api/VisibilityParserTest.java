package com.cwpad.datapoint.api;

import static org.junit.Assert.*;

import org.junit.Test;

public class VisibilityParserTest {
	@Test
	public void lessThan1kmNumeric() {
		assertEquals(Visibility.VP, Visibility.parse("500"));
	}

	@Test
	public void lessThan1kmWord() {
		assertEquals(Visibility.VP, Visibility.parse("VP"));
	}
	
	@Test
	public void moreThan40kmNumeric() {
		assertEquals(Visibility.EX, Visibility.parse("50000"));
	}

	@Test
	public void moreThan40kmWord() {
		assertEquals(Visibility.EX, Visibility.parse("EX"));
	}
	
	@Test
	public void moderate4km() {
		assertEquals(Visibility.MO, Visibility.parse("4000"));
	}
	
	@Test
	public void poor3_9km() {
		assertEquals(Visibility.PO, Visibility.parse("3999"));
	}

}
