package com.cwpad.datapoint.services;

import com.cwpad.datapoint.entity.WeatherObservation;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class DatapointParserSingleLocationTest {
	private DatapointObservationParser parser;
	private List<WeatherObservation> observations;
	
	@Before
	public void setUp() throws IOException {
		try (Reader reader = new InputStreamReader(Thread.currentThread().getContextClassLoader()
		.getResourceAsStream("samples/wxobs/3238.json"))) {
			parser = new DatapointObservationParser(reader);
			observations = parser.parse();
		}
	}
	
	@Test
	public void count() {
		assertEquals(25, observations.size());
	}
}
