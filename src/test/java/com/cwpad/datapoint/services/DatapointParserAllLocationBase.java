package com.cwpad.datapoint.services;

import com.cwpad.datapoint.entity.WeatherObservation;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class DatapointParserAllLocationBase {
    DatapointObservationParser parser;
    List<WeatherObservation> observations;

    public void configure(String path) throws IOException {
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        try (InputStream in  = cl.getResourceAsStream(path)) {
            parser = new DatapointObservationParser(new InputStreamReader(in));
            observations = parser.parse();
        }
    }
}
