package com.cwpad.datapoint.services;

import org.hsqldb.jdbc.JDBCDataSource;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.function.Consumer;

import static org.junit.Assert.*;

public class ObservationDownloaderIT {
	private static final JDBCDataSource dataSource = new JDBCDataSource();
	static {
		dataSource.setUrl("jdbc:hsqldb:mem:test1");
		dataSource.setUser("sa");
		dataSource.setPassword("");
	}
	private static EntityManager entityManager;

	@BeforeClass
	public static void setUp() throws NamingException, IOException {
		InitialContext context = BasicInitialContextFactory.newInitialContext();
		context.bind("jdbc/datapoint", dataSource);

		Map<String, String> properties = new HashMap<>();
		properties.put("javax.persistence.transactionType", "RESOURCE_LOCAL");
		properties.put("javax.persistence.nonJtaDataSource", "jdbc/datapoint");
		properties.put("hibernate.hbm2ddl.auto", "create");
		//properties.put(LOGGING_LEVEL, "FINE");

		entityManager = Persistence.createEntityManagerFactory("datapoint",
				properties).createEntityManager();
		


		LocationRepositoryImpl locationRepository = new LocationRepositoryImpl(
				entityManager);

		WeatherObservationRepositoryImpl weatherObservationRepository = new WeatherObservationRepositoryImpl(
				entityManager);
		Properties config = new Properties();
		config.setProperty(
				ObservationDownloader.DATAPOINT_OBS_URL_KEY,
				"http://datapoint.metoffice.gov.uk/public/data/"
						+ "val/wxobs/all/json/3238?res=hourly&key=5123c563-0cbc-46df-84d1-1e9595e692bf");

		ObservationDownloader downloader = new ObservationDownloader(locationRepository, weatherObservationRepository,
				config);

		entityManager.getTransaction().begin();
		downloader.parse();
		entityManager.getTransaction().commit();

	}

	@Test
	public void locationIsSaved() throws IOException, SQLException {
		executeSelect("select count(*) from Location", rs -> {
			try {
				assertEquals(1, rs.getInt(1));
			} catch (SQLException e) {
				e.printStackTrace();
				fail();
			}
		});
	}

	@Test
	public void observationsAreSaved() throws IOException, SQLException {
		executeSelect("select count(*) from WeatherObservation", rs -> {
			try {
				assertTrue(rs.getInt(1) >= 1);
			} catch (SQLException e) {
				e.printStackTrace();
				fail();
			}
		});
	}

	void executeSelect(String selectStmt,
			Consumer<ResultSet> resultSetProcessor) throws SQLException {
		try (Connection connection = dataSource.getConnection()) {
			try (Statement stmt = connection.createStatement()) {
				try (ResultSet rs = stmt.executeQuery(selectStmt)) {
					while (rs.next()) {
						resultSetProcessor.accept(rs);
					}
				}
			}
		}
	}
}
