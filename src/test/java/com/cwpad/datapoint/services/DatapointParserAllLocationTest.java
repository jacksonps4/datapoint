package com.cwpad.datapoint.services;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class DatapointParserAllLocationTest extends DatapointParserAllLocationBase {
	@Before
	public void setUp() throws IOException {
		configure("samples/wxobs/wxobs-all.pp.json");
	};

	@Test
	public void count() {
		assertEquals(4340, observations.size());
	}
}
