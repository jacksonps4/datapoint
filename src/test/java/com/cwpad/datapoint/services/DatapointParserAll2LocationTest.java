package com.cwpad.datapoint.services;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class DatapointParserAll2LocationTest extends DatapointParserAllLocationBase {
	@Before
	public void setUp() throws IOException {
		configure("samples/wxobs/wxobs-all2.pp.json");
	};

	@Test
	public void count() {
		assertEquals(4345, observations.size());
	}
}
